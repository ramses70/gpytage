#!/usr/bin/env python3

"""
    GTK Utility to help manage Portage's user config files

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import sys
import logging
from pathlib import Path
from getopt import getopt
from getopt import GetoptError


def usage():
    tabs = "\t\t"
    print("Usage: gpytage [OPTION...]\n")
    print("  -h, --help" + tabs + "Show this help message")
    print("  -l, --local" + tabs +
          "Run a local version (use modules in current directory)")
    print("  -v, --version" + tabs + "Output version information and exit")
    print("  -t, --test" + tabs + "Use the pre-configured test directory instead of '/etc/portage'")


if __name__ == "__main__":
    try:
        opts, args = getopt(sys.argv[1:], 'lvth', ["local", "version", "test", "help"])
    except GetoptError as e:
        print(e.msg, file=sys.stderr)
        usage()
        sys.exit(1)

    RUN_LOCAL = False

    logger = logging.getLogger('gpytage')
    logger.setLevel(logging.DEBUG)

    # get user directory for logs
    home = str(Path.home()) + '/.config/gpytage'
    logfile = home + '/gpytage.log'
    if os.path.isdir(home):
        if os.path.isfile(logfile):
            os.remove(logfile)
    else:
        os.mkdir(home)

    fh = logging.FileHandler(logfile)
    fh.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(fh)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        if opt in ("-l", "--local"):
            # running a local version (i.e. not installed in /usr/*)
            import os
            import os.path

            DATA_PATH = (os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
            sys.path.insert(0, DATA_PATH)
            RUN_LOCAL = True
            from gpytage.version import version
            from gpytage.startup import *

            # local()
            logger.debug("GPytage: starting gpytage()")

    if not RUN_LOCAL:  # then run the installed modules
        try:
            from gpytage.version import version
            from gpytage.startup import *

            logger.debug("GPytage: starting gpytage()")
        except ImportError as e:
            print("*** Error loading gpytage modules!\n*** If you are running a",
                  "local (not installed in python's site-packages) version, please use the '--local'",
                  "or '-l' flag.\n",
                  "*** Otherwise, verify that gpytage was installed correctly and",
                  "that python's path includes the site-packages directory.\n",
                  "If you have recently updated python, then run 'python-updater'\n")
            print("Your sys.path: %s\n" % sys.path)
            print("Your sys.version: %s\n" % sys.version)
            print("Original exception was: ImportError: %s\n" % e)
            sys.exit()

    for opt, arg in opts:  # broken for now
        if opt in ("-t", "--test"):
            config.set_test_path()

    for opt, arg in opts:
        # print "opt, arg ", opt, arg, type(arg)
        if opt in ("-v", "--version"):
            # print version info
            print("GPytage " + version)
            sys.exit(0)

    gpytagemain = GpytageMain()
    gpytagemain.main()
