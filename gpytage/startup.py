#!/usr/bin/env python3

"""
    GPytage startup.py module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import sys
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from gpytage import config
from gpytage.window import getTitleState
from gpytage.window import unsavedDialog
from gpytage.version import version
from gpytage.datastore import initTreeModel
from gpytage.datastore import initData
from gpytage.newFile import newFile
from gpytage.fileOperations import saveModifiedFile
from gpytage.fileOperations import saveModifiedFiles
from gpytage.fileOperations import revertSelected
from gpytage.fileOperations import revertAllModified
from gpytage.deleteFile import deleteFile
from gpytage.rename import renameFile
from gpytage.leftview import LeftView
from gpytage.rightview import RightView
from gpytage.windowid import WindowID

logger = logging.getLogger(__name__)

# set global defaults
DATA_PATH = "/usr/share/gpytage/"
PIXMAPS = "/usr/share/gpytage/pixmaps/"
GLADE_PATH = "/usr/share/gpytage/glade/"


def local():
    """ set global defaults for running locally """
    global DATA_PATH, PIXMAPS, GLADE_PATH
    DATA_PATH = os.path.dirname(os.path.abspath(__file__))
    PIXMAPS = os.path.join(DATA_PATH, "pixmaps/")
    GLADE_PATH = os.path.join(DATA_PATH, "glade/")


import os.path

location = os.path.abspath(__file__)
if "site-packages" not in location:
    local()


# Somehow, this line breaks deleteFile.py's shutil.rmtree
# del os.path

class GpytageMain(WindowID):
    def __init__(self):
        WindowID.__init__(self)
        logger.debug("STARTUP: DATA_PATH:  %s", DATA_PATH)
        logger.debug("STARTUP: PIXMAPS:    %s", DATA_PATH)
        logger.debug("STARTUP: GLADE_PATH: %s", GLADE_PATH)

        self.gladefile = GLADE_PATH + 'gpytage.glade'
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)  # loads all objects defined in main_window.glade

        initData()
        initTreeModel()

        self.mainwindow = self.builder.get_object("main_window")

        # WindowID class for title setting
        WindowID.windowID = self.mainwindow
        WindowID.editedState = False

        # !!!! keep next 8 lines in this order, see next
        self.right_view = RightView()
        right_scroll = self.builder.get_object("rScroll")
        right_scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        right_scroll.add_with_viewport(self.right_view)

        self.left_view = LeftView(self.right_view)  # <-- right view passed to left view
        left_scroll = self.builder.get_object("lScroll")
        left_scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        left_scroll.add(self.left_view)

        # setup WindowID variables
        WindowID.leftpanel = self.left_view
        WindowID.rightpanel = self.right_view

        handlers = {
            "on_main_window_destroy": self.goodbye,
            "newFile": newFile,
            "saveModifiedFile": saveModifiedFile,
            "saveModifiedFiles": saveModifiedFiles,
            "revertSelected": revertSelected,
            "revertAllModified": revertAllModified,
            "deleteFile": deleteFile,
            "renameFile": renameFile,
            "gpytageQuit": self.destroy,
            "insertRow": self.right_view.insertRow,
            "deleteRow": self.right_view.deleteRow,
            "toggleComment": self.right_view.toggleComment,
            "expandRows": self.left_view.expandRows,
            "collapseRows": self.left_view.collapseRows,
            "gpyAbout": self.about,
            "commentRow": self.right_view.commentRow,
            "uncommentRow": self.right_view.uncommentRow,
            "extedit": self.spawn_edit,
            "gosu": self.spawn_su,
        }
        self.builder.connect_signals(handlers)

        # allow the program to quit
        self.mainwindow.connect("destroy", self.destroy)
        self.mainwindow.connect("delete_event", self.delete_event)
        self.mainwindow.show_all()

    def spawn_edit(self, *args):
        from gpytage.helper import getCurrentFile
        file, model = getCurrentFile()
        if file is not None:
            edit_file = "gedit " + file.getPath() + "&"
            os.system(edit_file)

    def spawn_su(self, *args):
        """Not really working yet - respawn sudo for root access """
        euid = os.geteuid()
        if euid != 0:
            args = ['sudo', sys.executable] + sys.argv + [os.environ]
            # the next line replaces the currently-running process with the sudo
            os.execlpe('sudo', *args)

    def destroy(self, widget, data=None):
        state = getTitleState()
        logger.debug("STATE IS:  %s", str(state))
        if state is True:  # There are edited files
            status, unSaved = unsavedDialog()
            if status == -8:  # YES
                Gtk.main_quit()
            else:  # NO and other unhandled signals
                unSaved.hide()
        else:
            Gtk.main_quit()

    def delete_event(self, widget, event, data=None):
        self.destroy(widget)
        return True

    # Menu Functions
    def about(self, *args):
        aboutwindow = Gtk.AboutDialog()
        aboutwindow.set_program_name('GPytage 3')
        aboutwindow.set_copyright('Copyright 2008-2020, GPL2')
        aboutwindow.set_authors(["Kenneth 'ken69267' Prugh",
                                 "\nConverted to Python 3 and GTK3 by Michael Greene\n" +
                                 "\nWith patches contributed by Brian Dolbec <dol-sen>\nand Josh 'nightmorph' Saddler. \n" +
                                 "\nWith special thanks to the Gentoo \ndevelopers and community. \n\n" +
                                 "Licensed under the GPL-2"])  # Fix wording? :)

        # get license text from portage directory
        flicense = open(config.PORTDIR + '/licenses/GPL-2')
        gpl2 = flicense.read()
        flicense.close()
        aboutwindow.set_license(gpl2)
        aboutwindow.set_wrap_license(True)
        aboutwindow.set_version(version)
        aboutwindow.set_website('https://gna.org/projects/gpytage/')
        try:
            self.i128 = GdkPixbuf.Pixbuf.new_from_file(PIXMAPS + "gpytage-128x128.png")
            aboutwindow.set_logo(self.i128)
        except:
            logger.debug("ABOUT(): Logo could not be set")
        aboutwindow.run()
        aboutwindow.hide()

    def goodbye(self, *widget):
        """
        GtkWindow destory signal window X
        Main window quit function"""
        logger.debug("MAINWINDOW: goodbye(); quiting now")
        Gtk.main_quit()

    def main(self):
        Gtk.main()
