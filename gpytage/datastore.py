#!/usr/bin/env python3

"""
    GPytage datastore.py module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gpytage import FolderObj
from gpytage import PackageFileObj
from gpytage.config import config_files
from gpytage.config import get_config_path

logger = logging.getLogger(__name__)

# declare some constants for clarity of code
F_NAME = 0
F_REF = 1

# Folders            entry name, 1 entry reference
folderModel = Gtk.TreeStore(str, object)

TLFolders = []  # Holds *ALL* folders, check for children
TLFiles = []

addedPaths = []

def initData():
    """ Constructs Folder and PackageFile objects """
    # get portage config --- ex: /etc/portage/ for my older system
    portpath = get_config_path()
    logger.debug("DATASTORE:  Using path:  %s", portpath)
    if addedPaths:
        logger.debug("DATASTORE:  Using extra paths:  %s", addedPaths)

    for rootDir, folders, files in os.walk(portpath, topdown=True):
        # Begin the construction
        if rootDir is portpath:  # we are top level, no children
            # Filter unrelated files TOPLEVEL ONLY
            # Folders sanity
            for folder in folders[:]:
                if folder not in config_files:
                    folders.remove(folder)  # ignore unrelated folders
            # Files sanity
            for file in files[:]:
                if file not in config_files:
                    files.remove(file)  # ignore unrelated files
            # construct
            for folder in folders:
                foldobj = FolderObj.FolderObj(folder, rootDir + folder)
                TLFolders.append(foldobj)
            for file in files:
                fileobj = PackageFileObj.PackageFileObj(file, rootDir + file, None)
                TLFiles.append(fileobj)
        else:  # No longer top level, we are inside a folder
            tlname = rootDir.split("/")[-1]  # /etc/portage/sets => sets
            # construct
            for folder in folders:  # recursive folder
                parent = __getParent(tlname, "folder")  # FolderObj
                foldobj = FolderObj.FolderObj(folder, rootDir + "/" + folder)
                # ToDo args were not used always parent true from here in sub directory
                foldobj.setHasParent(True)
                foldobj.setParentFolder(parent)
                parent.addFolder(foldobj)
                parent.setChildren(True)
                TLFolders.append(foldobj)
            for file in files:
                parent = __getParent(tlname, "file")  # PackageFileObj
                fileobj = PackageFileObj.PackageFileObj(file, rootDir + "/" + file, parent)
                parent.addPackage(fileobj)
                # TLFiles.append(fileobj)
    # handle save as files in root of right panel
    for x in range(0, len(addedPaths)):
        fileobj = PackageFileObj.PackageFileObj(addedPaths[x][1], addedPaths[x][0] + "/" + addedPaths[x][1], None)
        fileobj.saveasFile = True
        TLFiles.append(fileobj)


def __getParent(tlname, type):
    """ Finds the associated parent named tlname from the specified list type """
    if type is "folder":
        for folder in TLFolders:
            if folder.getName() == tlname:
                return folder
    if type is "file":
        for folder in TLFolders:
            if folder.getName() == tlname:
                return folder


def initTreeModel():
    """ Populate the TreeModel with data """
    for folder in TLFolders:  # Contains *all* folders
        # Handle Top level Folders with no folder children first
        if (folder.getChildrenState() == False and folder.getParentState() == False):

            row = [folder.getName(), folder]
            parentIter = folderModel.append(None, row)
            path = folderModel.get_path(parentIter)
            treeRowRef = Gtk.TreeRowReference(folderModel, path)
            folder.setTreeRowRef(treeRowRef)
            children = folder.getPackages()
            for child in children:  # Add children files to treeview
                row = [child.getName(), child]
                childIter = folderModel.append(parentIter, row)
                path = folderModel.get_path(childIter)
                treeRowRef = Gtk.TreeRowReference(folderModel, path)
                child.setTreeRowRef(treeRowRef)
        else:  # Folders have folder children (an unknown amount unfortunately) (Recursive)
            # Add the parent folder to the treeview
            row = [folder.getName(), folder]
            if folder.getParentState() == False:  # Top level
                parentIter = folderModel.append(None, row)
                path = folderModel.get_path(parentIter)
                treeRowRef = Gtk.TreeRowReference(folderModel, path)
                folder.setTreeRowRef(treeRowRef)  # We will need this later to pack the children
                # in the treeview
            else:  # child folder
                # We gotta find the stupid things parent row
                parent = folder.getParentFolder()
                path = parent.getTreeRowRef().get_path()
                grandIter = folderModel.get_iter(path)
                parentIter = folderModel.append(grandIter, row)
                path = folderModel.get_path(parentIter)
                treeRowRef = Gtk.TreeRowReference(folderModel, path)
                folder.setTreeRowRef(treeRowRef)

            for subfile in folder.getPackages():
                row = [subfile.getName(), subfile]
                FileIter = folderModel.append(parentIter, row)
                path = folderModel.get_path(FileIter)
                treeRowRef = Gtk.TreeRowReference(folderModel, path)
                subfile.setTreeRowRef(treeRowRef)
    # Top Level Files only
    for file in TLFiles:
        row = [file.getName(), file]
        parentIter = folderModel.append(None, row)
        path = folderModel.get_path(parentIter)
        treeRowRef = Gtk.TreeRowReference(folderModel, path)
        file.setTreeRowRef(treeRowRef)


def __clearData():
    """ Clears the TreeModel and the TLFolder,TLFiles list """
    folderModel.clear()
    del TLFolders[:]
    del TLFiles[:]


def reinitializeDatabase():
    """ Clears the backend and rebuilds the database from disk """
    from gpytage.windowid import WindowID
    __clearData()
    initData()
    initTreeModel()
    WindowID.rightpanel.setListModel(None)


def getsaveasData(filename):
    """ Return object for saveas filename """
    for file in TLFiles:
        if filename is file.getName():
            logger.debug("SAVEAS: Object found %s", file)
            return file


def delsaveasData(data):
    """ Remove SaveAs file from scan """
    filename = data.getName()
    for item in addedPaths:
        if item[1] is filename:
            addedPaths.remove(item)
