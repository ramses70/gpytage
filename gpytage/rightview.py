#!/usr/bin/env python3

"""
    GPytage rightpanel.py module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import sys
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gpytage.helper import getMultiSelection
from gpytage.helper import getCurrentFile
from gpytage.PackageFileObj import L_NAME, L_FLAGS, L_REF
from gpytage.fileOperations import fileEdited
from gpytage.window import clipboard

logger = logging.getLogger(__name__)


class RightView(Gtk.TreeView):

    def __init__(self):
        """ Initialize """
        Gtk.TreeView.__init__(self)

        # return Gtk.TreeSelection
        self.rightselection = self.get_selection()

        # Any number of elements may be selected. The Ctrl key may be used to enlarge
        # the selection, and Shift key to select between the focus and the child pointed
        # to. Some widgets may also allow Click-drag to select a range of elements.
        self.rightselection.set_mode(Gtk.SelectionMode.MULTIPLE)

        # Currently interfering with copy/paste/cut
        # rightview.set_search_column(L_NAME)
        self.set_enable_search(False)

        # TreeViewColumns
        self.namecol = Gtk.TreeViewColumn('Package')
        self.useFlagCol = Gtk.TreeViewColumn('Flags')

        # Add TreeViewColumns to TreeView
        self.append_column(self.namecol)
        self.append_column(self.useFlagCol)

        # CellRenderer construction
        self.nameCell = myCellRendererText(0)
        self.nameCell.set_property('editable', True)
        self.flagCell = myCellRendererText(1)
        self.flagCell.set_property('editable', True)

        # Add CellRenderer to TreeViewColumns
        self.namecol.pack_start(self.nameCell, True)
        self.namecol.add_attribute(self.nameCell, 'text', L_NAME)
        self.namecol.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)

        self.useFlagCol.pack_start(self.flagCell, True)
        self.useFlagCol.add_attribute(self.flagCell, 'text', L_FLAGS)
        self.useFlagCol.set_expand(True)
        self.useFlagCol.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)

        # Signals
        self.nameCell.connect("edited", self.edited_cb, L_NAME)
        self.flagCell.connect("edited", self.edited_cb, L_FLAGS)
        self.connect("button_press_event", self.__rightClicked)
        self.connect("key_press_event", self.__handleKeyPress)
        logger.debug("RIGHTVIEW: initialization complete.")

    # ### Right Panel handlers
    def insertRow(self, arg):
        """ Insert row below selected row(s) """
        rowReferences = getMultiSelection(self)
        try:
            lastRowSelectedPath = rowReferences[-1].get_path()
            model = rowReferences[-1].get_model()
        except:
            # Well, we got a blank file
            PackageFile, lModel = getCurrentFile()
            if PackageFile == None:  # No file is actually selected
                return
            model = self.get_model()
            if model == None:  # A folder is selected
                return
            newRow = model.append(["", "", PackageFile])
            # Set the cursor on the new row and start editing the name column
            path = model.get_path(newRow)
            self.set_cursor(path, self.namecol, True)
            # Fire off the edited methods
            fileEdited(PackageFile)
            return

        lastRowIter = model.get_iter(lastRowSelectedPath)
        # We need to link this new row with its PackageFile Object
        PackageFile = model.get_value(lastRowIter, L_REF)
        # Insert into the model
        newRow = model.insert_after(lastRowIter, ["", "", PackageFile])
        # Set the cursor on the new row and start editing the name column
        path = model.get_path(newRow)
        self.set_cursor(path, self.namecol, True)
        # Fire off the edited methods
        fileEdited(PackageFile)

    def deleteRow(self, arg):
        """ Delete selected row(s) """
        rowReferences = getMultiSelection(self)
        model = self.get_model()
        PackageFile, lModel = getCurrentFile()
        for ref in rowReferences:
            iter = model.get_iter(ref.get_path())
            model.remove(iter)
        # If nothing is deleted we shouldn't show
        if len(rowReferences) > 0:
            fileEdited(PackageFile)

    def toggleComment(self, *args):
        """ Toggle comments on selected rows based on the first row """
        rowReferences = getMultiSelection(self)
        model = self.get_model()
        file, lModel = getCurrentFile()
        comment = True
        # Is anything even selected?
        if len(rowReferences) == 0:
            return
        # Lets see what the first comment is
        ref = rowReferences[0]
        iter = model.get_iter(ref.get_path())
        cText = model.get_value(iter, L_NAME)
        if cText.startswith("#"):
            comment = False
        # Lets proceed now
        if comment:  # Comment all lines
            for ref in rowReferences:
                iter = model.get_iter(ref.get_path())
                cText = model.get_value(iter, L_NAME)
                if not cText.startswith("#"):
                    model.set_value(iter, L_NAME, "#" + cText)
        else:  # Uncomment all lines
            for ref in rowReferences:
                iter = model.get_iter(ref.get_path())
                cText = model.get_value(iter, L_NAME)
                if cText.startswith("#"):
                    model.set_value(iter, L_NAME, cText[1:])
        fileEdited(file)

    def commentRow(self, window):
        """ Comment selected row(s) """
        rowReferences = getMultiSelection(self)
        model = self.get_model()
        PackageFile, lModel = getCurrentFile()
        # Is anything even selected?
        if len(rowReferences) == 0:
            return
        for ref in rowReferences:
            iter = model.get_iter(ref.get_path())
            cText = model.get_value(iter, L_NAME)
            if not cText.startswith("#"):
                model.set_value(iter, L_NAME, "#" + cText)
        fileEdited(PackageFile)

    def uncommentRow(self, window):
        """ Uncomment selected row(s) """
        rowReferences = getMultiSelection(self)
        model = self.get_model()
        PackageFile, lModel = getCurrentFile()
        # Is anything even selected?
        if len(rowReferences) == 0:
            return
        for ref in rowReferences:
            iter = model.get_iter(ref.get_path())
            cText = model.get_value(iter, L_NAME)
            if cText.startswith("#"):
                model.set_value(iter, L_NAME, cText[1:])
        fileEdited(PackageFile)

    def __rightClicked(self, view, event):
        """ Right click menu for package options """
        if event.button == 3:
            menu = Gtk.Menu()
            irow = Gtk.MenuItem("Insert Package")
            irow.connect("activate", self.insertRow)
            menu.append(irow)
            drow = Gtk.MenuItem("Delete Package")
            drow.connect("activate", self.deleteRow)
            menu.append(drow)
            separator = Gtk.MenuItem()
            menu.append(separator)
            crow = Gtk.MenuItem("Comment Package")
            crow.connect("activate", self.commentRow)
            menu.append(crow)
            urow = Gtk.MenuItem("Uncomment Package")
            urow.connect("activate", self.uncommentRow)
            menu.append(urow)
            cut = Gtk.MenuItem("Cut")
            cut.connect("activate", self.__menuCut)
            menu.append(cut)
            copy = Gtk.MenuItem("Copy")
            copy.connect("activate", self.__menuCopy)
            menu.append(copy)
            paste = Gtk.MenuItem("Paste")
            paste.connect("activate", self.__menuPaste)
            menu.append(paste)
            menu.show_all()
            menu.popup(None, None, None, None, event.button, event.time)

    # Callbacks
    def edited_cb(self, cell, path, new_text, col):
        """ Indicate file has been edited """
        model = self.get_model()
        model[path][col] = new_text
        file = model[path][L_REF]
        # Indicate file status in TreeView
        fileEdited(file)

    # Methods that __rightClicked can call for cut/copy/paste
    def __menuCut(self, *args):
        clipboard.cutToClipboard(self)

    def __menuCopy(self, *args):
        clipboard.copyToClipboard(self)

    def __menuPaste(self, *args):
        clipboard.pasteClipboard(self)

    def __handleKeyPress(self, widget, event):
        modifiers = Gtk.accelerator_get_default_mod_mask()
        # copy
        if event.keyval == Gdk.keyval_from_name("c"):
            if (modifiers & event.state) == Gdk.ModifierType.CONTROL_MASK:
                clipboard.copyToClipboard(self)
        # paste
        if event.keyval == Gdk.keyval_from_name("v"):
            if (modifiers & event.state) == Gdk.ModifierType.CONTROL_MASK:
                clipboard.pasteClipboard(self)
        # cut
        if event.keyval == Gdk.keyval_from_name("x"):
            if (modifiers & event.state) == Gdk.ModifierType.CONTROL_MASK:
                clipboard.cutToClipboard(self)

    def setListModel(self, ListStore):  # we need to switch the model on click
        if ListStore == None:
            logger.debug("RIGHTVIEW:  setListModel(); Folder")
            return
        try:
            logger.debug("RIGHTVIEW:  setListModel(); File")
            self.set_model(None)  # Clear from view first
            self.set_model(ListStore)  # example
            cFormatter = SplitComments(self)    # self.__splitComments(self)
            cFormatter.format()
            del cFormatter
            self.namecol.queue_resize()
            logger.debug("RIGHTVIEW:  setListModel(); finished file object  %s", sys.stderr)
        except:
            logger.debug("RIGHTVIEW:  setListModel(); failed  %s", sys.stderr)


class SplitComments():
    """ Used to properly format long comment strings inside portage config
        files """

    def __init__(self, rightview):
        self.largest = 0
        self.rightview = rightview
        self.model = rightview.get_model()
        self.commentIters = []
        self.model.foreach(self.getNormalizedLength)

    def format(self):
        """ If the comment iter is longer than self.largest, the comment is
            split into 2 columns at a word, if the comment is a single line
            with no word breaks a split is done at self.largest"""
        for iter in self.commentIters:
            comment = self.model.get_value(iter, 0).strip()
            if len(comment) > self.largest and self.largest != 0:
                # Comment is larger than the longest standard line which means
                # we should break the excess length into the second column for
                # the comment, split at a word
                try:
                    split = comment.rindex(" ", 0, self.largest + 1)
                    c1 = comment[:split].strip()
                    c2 = comment[split:].strip()
                    self.model.set_value(iter, 0, c1)
                    self.model.set_value(iter, 1, c2)
                except ValueError:
                    # We couldn't break at a word
                    c1 = comment[:self.largest].strip()
                    c2 = comment[self.largest:].strip()
                    self.model.set_value(iter, 0, c1)
                    self.model.set_value(iter, 1, c2)

    def getNormalizedLength(self, model, path, iter):
        """ Records the largest length of a entry that is not a comment and
            stores those which are comments in self.commentIters """
        colOne = model.get_value(iter, 0).strip()
        # Non-comment line
        if not colOne.startswith("#"):
            size = len(colOne)
            if size > self.largest:
                self.largest = size
        else:
            # A comment line
            self.commentIters.append(iter)


class myCellRendererText(Gtk.CellRendererText):
    """ class to have tab behave when editing """

    def __init__(self, colNum):
        Gtk.CellRendererText.__init__(self)
        self.connect("editing-started", self.editing)
        self.col = colNum

    def editing(self, cell, editable, path):
        editable.connect("key-press-event", self.key)
        return False

    def key(self, widget, event):
        if event.keyval == Gdk.keyval_from_name("Tab"):
            from gpytage.windowid import WindowID

            # Save any current editing changes to the cell
            widget.editing_done()
            # Now it's time to move our edit to the next appropriate cell
            # If we are editing col 1 of a row, we should switch to editing col2, but
            # if we are editing col2 of a row, we should switch to editing col1 of the
            # next row
            model = self.get_model()
            refs = getMultiSelection(WindowID.rightpanel)
            path = refs[-1].get_path()
            if self.col == L_NAME:  # We are editing col 1
                self.rightview.set_cursor_on_cell(path, self.rightview.get_column(L_FLAGS), None, True)
            elif self.col == L_FLAGS:  # We are editing col2
                nextRow = model.iter_next(model.get_iter(path))  # iter to next row
                if nextRow != None:
                    self.rightview.set_cursor_on_cell(model.get_path(nextRow),
                                                      self.rightview.get_column(L_NAME), None, True)
