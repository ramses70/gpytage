#!/usr/bin/env python3

"""
    GPytage config.py module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import logging
from sys import exit

logger = logging.getLogger(__name__)

# A current list: https://wiki.gentoo.org/wiki//etc/portage
config_files = ['bashrc', 'categories', 'color.map', 'mirrors', 'modules',
                'package.keywords', 'package.accept_keywords', 'package.license', 'package.mask',
                'package.properties', 'package.unmask', 'package.use',
                'repos.conf', 'profile', 'sets']

test_path = '/etc/testportage/'

"""
The original code had routines for checking for for Portage 2.1 or => 2.2 not much use 
considering low version is way past in portage as of today. 
"""
try:
    import portage
    import portage.const as portage_const
except ModuleNotFoundError:
    exit('Could not find portage module. Are you sure this is a Gentoo system?')

logger.debug("Config: portage version = %s", portage.VERSION)

# portage config files
portage_path = "/" + portage_const.USER_CONFIG_PATH
config_path = portage_path + '/'
# portage repos
# noinspection PyUnresolvedReferences
PORTDIR = portage.config(clone=portage.settings).environ()['PORTDIR']


def set_test_path():
    global config_path, test_path
    config_path = test_path
    logger.debug("CONFIG: new config_path = %s", config_path)


def get_config_path():
    return config_path
