#!/usr/bin/env python3

"""
    GPytage file object

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import sys
import logging
from gpytage.window import setTitleEdited
from gpytage.PackageFileObj import L_NAME
from gpytage.FolderObj import FolderObj
from gpytage.window import saveasDialog
from gpytage.config import config_files
from gpytage.datastore import addedPaths
from gpytage.datastore import reinitializeDatabase
from gpytage.datastore import getsaveasData

logger = logging.getLogger(__name__)

# List of files that have been edited and need saving or reverting
modifiedFiles = []


def __appendModifiedFile(file):
    if file not in modifiedFiles:
        modifiedFiles.append(file)


def __removeModifiedFile(file):
    if file in modifiedFiles:
        modifiedFiles.remove(file)


def fileEdited(file):
    """ Set the passed PackageFileObj as edited """
    file.setEditedState(True)
    lpath = file.getTreeRowRef().get_path()
    lmodel = file.getTreeRowRef().get_model()
    treename = lmodel[lpath][L_NAME]
    if treename == file.getName():
        # mark as edited
        lmodel[lpath][L_NAME] = "*" + treename
    # Reflect in title
    setTitleEdited(True)
    # Add to the modifiedFiles
    __appendModifiedFile(file)


# TODO: Test if files are writable os.access before proceeding further in the
#       save routine?
def saveModifiedFiles(*args):
    """ Saves all modified files """
    for file in modifiedFiles[:]:
        __saveFile(file)


def saveModifiedFile(*args):
    """ Saves the file currently selected """
    # Discover file currently selected
    from gpytage.helper import getCurrentFile
    file, model = getCurrentFile()
    # save it
    if file in modifiedFiles:
        __saveFile(file)


def saveasModifiedFile(*args):
    """ Saves the file currently selected
        to location with current permissions"""
    # Discover file currently selected
    from gpytage.helper import getCurrentFile
    file, model = getCurrentFile()
    if isinstance(file, FolderObj):
        logger.debug("SAVEFILE: folder selected - cancelled")
        return
    basefile = os.path.basename(file.getPath())
    logger.debug("SAVEAS: Attempting to save %s", file.getPath())
    newFile = saveasDialog(basefile)
    __saveFile(file, newFile)


def __saveFile(file, newFile = None):
    """ Private method to write file to desk """
    # Save the file
    if newFile is None:
        logger.debug("SAVEFILE: Attempting to save %s", file.getPath())
    else:
        logger.debug("SAVEFILE: Save As %s to new file %s", file.getPath(), newFile)
    try:
        if newFile is None:
            f = open(file.getPath(), 'w')
        else:
            f = open(newFile, 'w')
        datalist = file.getData()
        for row in datalist:
            if row[0] is not None:
                c1 = row[0]
            else:
                c1 = ""
            if row[1] is not None:
                c2 = row[1]
            else:
                c2 = ""
            if row[0].strip().startswith("#"):
                f.write(c1 + c2 + "\n")
            else:
                f.write(c1 + " " + c2 + "\n")
        f.close()

        if newFile is not None:
            head_tail = os.path.split(newFile)
            if head_tail[1] not in config_files:
                config_files.append(head_tail[1])
                addedPaths.append(head_tail)
                reinitializeDatabase()
                file = getsaveasData(head_tail[1])
        __fileSaved(file)
    except IOError as e:
        print("Error saving: ", e, file=sys.stderr)


def __fileSaved(file):
    """ Set the passed PackageFileObj as un-edited """
    logger.debug("__fileSaved %s", file)
    file.setEditedState(False)
    lpath = file.getTreeRowRef().get_path()
    lmodel = file.getTreeRowRef().get_model()
    # mark as unedited
    lmodel[lpath][L_NAME] = file.getName()
    # remove from the modifiedFiles
    __removeModifiedFile(file)
    # Reflect in title
    if hasModified() is False:
        setTitleEdited(False)


def hasModified():
    """ Return whether the modifiedFiles list is empty """
    if len(modifiedFiles) == 0:
        return False
    else:
        return True


def revertSelected(*args):
    """ Reverts the currently selected file """
    # Discover file currently selected
    from gpytage.windowid import WindowID
    from gpytage.helper import getCurrentFile
    file, model = getCurrentFile()
    if file in modifiedFiles:
        file.initData()
        __fileSaved(file)  # Well, it is unedited...
        from gpytage.rightview import RightView
        file_data = file.getData()
        WindowID.rightpanel.setListModel(file_data)


def revertAllModified(*args):
    """ Reverts all files that have been modified """
    from gpytage.windowid import WindowID
    from gpytage.helper import getCurrentFile
    cfile, model = getCurrentFile()
    for file in modifiedFiles[:]:
        file.initData()
        __fileSaved(file)
        if file == cfile:
            from gpytage.rightview import RightView
            file_data = file.getData()
            WindowID.rightpanel.setListModel(file_data)
