#!/usr/bin/env python3

"""
    GPytage window.py module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gpytage.clipboard import Clipboard

clipboard = Clipboard()


def setTitleEdited(bool):
    """
    Changes the state of the title. Edited state causes the title to change to *GPytage.
    True: Sets as edited
    """
    from gpytage.windowid import WindowID
    if bool is True:
        WindowID.windowID.set_title("*GPytage")
        WindowID.editedState = True
    else:
        WindowID.windowID.set_title("GPytage")
        WindowID.editedState = False


def getTitleState():
    """ Returns if the title is in the edited state """
    from gpytage.windowid import WindowID
    return WindowID.editedState


# ******************** dialogs ********************

def createMessageDialog(parent, flags, type, buttons, mtitle, message_format):
    md = Gtk.MessageDialog(parent, flags, type, buttons, message_format)
    md.set_title(mtitle)
    md.run()
    md.destroy()


def unsavedDialog():
    """
    Spawn Generic Yes/No/Save Dialog when unsaved changes are present.
    YES returns -8. NO returns -9. Save returns 1.
    """
    from gpytage.windowid import WindowID
    unSaved = Gtk.MessageDialog(WindowID.windowID,
                                flags=0,
                                message_type=Gtk.MessageType.WARNING,
                                buttons=Gtk.ButtonsType.YES_NO,
                                text="You have unsaved changes, if you proceed these changes will be lost.\n\n Do you wish to Quit?", )

    unSaved.set_title("You have unsaved changes")
    # uD.set_default_response(Gtk.RESPONSE_NO)
    status = unSaved.run()
    return status, unSaved


def saveasDialog(filename):
    from gpytage.windowid import WindowID

    saveAs = Gtk.FileChooserDialog("Save File",
                                   WindowID.windowID,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT))
    saveAs.set_do_overwrite_confirmation(True)
    saveAs.set_modal(True)
    filename = filename + ".new"
    saveAs.set_current_name(filename)

    # saveAs.set_file(filename)
    saveAs.set_title("Save as ....")
    saveAs.set_default_response(Gtk.ResponseType.CANCEL)

    response = saveAs.run()

    if response == Gtk.ResponseType.ACCEPT:
        newfile = saveAs.get_filename()
        saveAs.destroy()
        return newfile
    saveAs.destroy()


# ToDo no usage found - probably could make this the common dialog MKG
def error_dialog(errors):
    """generic dialog to report file operation errors"""
    from gpytage.windowid import WindowID

    err = ',\n'.join(errors)
    message = "Please correct the problem(s) and try again.  It may include running gpytage as the root user\n\n" + err
    createMessageDialog(WindowID.windowID,
                        Gtk.DialogFlags.DESTROY_WITH_PARENT,
                        Gtk.MessageType.ERROR,
                        Gtk.ButtonsType.OK,
                        "Error Saving Files...",
                        message)
