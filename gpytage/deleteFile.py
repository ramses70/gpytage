#!/usr/bin/env python3

"""
    deleteFile.py GPytage module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import sys
from shutil import rmtree
from os import remove
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gpytage.window import createMessageDialog
from gpytage.fileOperations import hasModified
from gpytage.PackageFileObj import PackageFileObj
from gpytage.FolderObj import FolderObj
from gpytage.datastore import reinitializeDatabase
from gpytage.datastore import delsaveasData
from gpytage.windowid import WindowID

logger = logging.getLogger(__name__)

def deleteFile(*args):
    """ Deletes the currently selected file """
    if __ensureNotModified():

        # What exactly is currently selected?
        from gpytage.datastore import F_REF
        model, iter = WindowID.leftpanel.get_selection().get_selected()
        try:
            object = model.get_value(iter, F_REF)
            if isinstance(object, PackageFileObj):  # A file
                file = object
                logger.debug("DELETE file: %s", file.getName())
                logger.debug("Is a SAVE AS file: %s", str(file.saveasFile))
                dialog = Gtk.MessageDialog(WindowID.windowID,
                                           Gtk.DialogFlags.MODAL,
                                           Gtk.MessageType.WARNING,
                                           Gtk.ButtonsType.YES_NO,
                                           "This operation is irreversible, are you sure you want to delete " + file.getName() + "?")
                dialog.set_default_response(Gtk.ResponseType.NO)
                dialog.set_title("File removal...")
                response = dialog.run()
                if response == Gtk.ResponseType.YES:
                    logger.debug("Test delete file: %s", file.getPath())
                    __removeFile(file.getPath())
                    dialog.destroy()
                    # we need to remove the SaveAS file before rescan
                    if file.saveasFile:
                        delsaveasData(file)
                    reinitializeDatabase()
                    if file.getParentFolder() != None:
                        __reselectAfterDelete(file.getParentFolder().getPath())
                else:
                    dialog.destroy()
            elif isinstance(object, FolderObj):  # A folder
                folder = object
                dialog = Gtk.MessageDialog(WindowID.windowID,
                                           Gtk.DialogFlags.MODAL,
                                           Gtk.MessageType.WARNING,
                                           Gtk.ButtonsType.YES_NO,
                                           "This operation is irreversible, are you sure you want to delete directory " +
                                           folder.getName() + " and its contents?")
                dialog.set_default_response(Gtk.ResponseType.NO)
                dialog.set_title("Directory removal...")
                response = dialog.run()
                if response == Gtk.ResponseType.YES:
                    logger.debug("Test delete dir: %s", folder.getPath())
                    __removeDirectory(folder.getPath())
                    dialog.destroy()
                    reinitializeDatabase()
                    if folder.getParentState():
                        __reselectAfterDelete(folder.getParentFolder().getPath())
                else:
                    dialog.destroy()
        except TypeError as e:
            print("deleteFile: ", e, file=sys.stderr)


def __removeFile(path):
    logger.debug("__removeFile  %s", path)
    try:
        remove(path)
    except OSError as e:
        print(e, file=sys.stderr)


def __removeDirectory(path):
    try:
        rmtree(path)
    except IOError as e:
        print(e, file=sys.stderr)


def __ensureNotModified():
    if hasModified():
        # inform user to save
        createMessageDialog(WindowID.windowID,
                            Gtk.DialogFlags.DESTROY_WITH_PARENT,
                            Gtk.MessageType.ERROR,
                            Gtk.ButtonsType.OK,
                            "Modified File",
                            "A file cannot be deleted with unsaved changes. Please save your changes.")
        return False
    else:
        logger.debug("__ensureNotModified: True")
        return True


def __reselectAfterDelete(folderPath):
    """ Reselects the parent folder of the deleted object """
    from gpytage.windowid import WindowID
    model = WindowID.leftpanel.get_model()
    model.foreach(getMatch, [folderPath, WindowID.leftpanel])


def getMatch(model, path, iter, data):
    """ Obtain the match and perform the selection """
    testObject = model.get_value(iter, 1)  # col 1 stores the reference
    # clarify values passed in from data list
    folderPath = data[0]
    leftview = data[1]
    if testObject.getPath() == folderPath:
        # since we delete the object, we need to just expand to the folder we
        # were in
        leftview.expand_to_path(path)
        leftview.set_cursor(path, None, False)
        return True
    else:
        return False
