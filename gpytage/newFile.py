#!/usr/bin/env python3

"""
    newFile.py GPytage module

    Fixed for Python 3 - August 2020 Michael Greene

    Copyright (C) 2008-2009 by Kenneth Prugh
    ken69267@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""


import sys
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gpytage.config import get_config_path
from gpytage.fileOperations import hasModified
from gpytage.window import createMessageDialog
from gpytage.datastore import reinitializeDatabase
from gpytage.PackageFileObj import PackageFileObj
from gpytage.FolderObj import FolderObj
from gpytage.windowid import WindowID

logger = logging.getLogger(__name__)

def newFile(*args):
    """ Create a new file """
    if __ensureNotModified():
        nFile = __getNewFileChoice()
        if nFile is not None:
            __saveToDisk(nFile)
            reinitializeDatabase()
            __reselectAfterNew(nFile)


def __saveToDisk(nFile):
    logger.debug("NEWFILE: __saveToDisk Saving new file: %s", nFile)
    try:
        fh = open(nFile, 'w')
        fh.write("# " + nFile + "\n")
        fh.write("# Created by GPytage\n")
        fh.close()
    except IOError as e:
        print(e, file=sys.stderr)


def __ensureNotModified():
    logger.debug("NEWFILE: __ensureNotModified")
    if hasModified():
        # inform user to save
        createMessageDialog(WindowID.windowID,
                            Gtk.DialogFlags.DESTROY_WITH_PARENT,
                            Gtk.MessageType.ERROR,
                            Gtk.ButtonsType.OK,
                            "A new file cannot be created with unsaved changes. Please save your changes.")
        return False
    else:
        return True


# noinspection PyArgumentList
def __getNewFileChoice():
    dialog = Gtk.FileChooserDialog("New file...",
                                   WindowID.windowID,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT))

    dialog = Gtk.FileChooserDialog("New file...", None, Gtk.FileChooserAction.OPEN)
    dialog.add_buttons(
        Gtk.STOCK_CANCEL,
        Gtk.ResponseType.CANCEL,
        Gtk.STOCK_OPEN,
        Gtk.ResponseType.OK,
    )
    filter = Gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)
    response = dialog.run()

    if response == Gtk.ResponseType.OK:
        result = dialog.get_filename()
        logger.debug("NEWFILE: __getNewFileChoice %s", result)
    elif response == Gtk.ResponseType.CANCEL:
        logger.debug("NEWFILE: __getNewFileChoice Cancelled")
        result = None
    dialog.destroy()
    return result

    # Does the user have a folder selected that we should instead load on
    # newFile?
    model, iter = WindowID.leftpanel.get_selection().get_selected()
    from gpytage.datastore import F_REF
    try:
        object = model.get_value(iter, F_REF)
        if isinstance(object, PackageFileObj):  # A file
            folder = object.getParentFolder()
        elif isinstance(object, FolderObj):  # A folder
            folder = object
        if folder == None:
            folderPath = get_config_path()
        else:
            folderPath = folder.getPath()  # Get the path to the folder object
    except TypeError as e:
        print("__getNewFileChoice:", e, file=sys.stderr)
        # Nothing selected, select default
        folderPath = get_config_path()

    dialog.set_current_folder(folderPath)
    ## end logic

    dialog.set_do_overwrite_confirmation(True)
    response = dialog.run()
    name = None
    if response == Gtk.ResponseType.OK:
        name = dialog.get_filename()

    # close the dialog and return the filename
    dialog.destroy()
    return name


def __reselectAfterNew(filePath):
    """ Reselects the parent folder of the deleted object """
    model = WindowID.leftpanel.get_model()
    model.foreach(getMatch, [filePath, WindowID.leftpanel])


def getMatch(model, path, iter, data):
    logger.debug("NEWFILE: getMatch")
    """ Obtain the match and perform the selection """
    testObject = model.get_value(iter, 1)  # col 1 stores the reference
    # clarify values passed in from data list
    filePath = data[0]
    leftview = data[1]
    if testObject.getPath() == filePath:
        # We found the file object we just added, lets select it
        leftview.expand_to_path(path)
        leftview.set_cursor(path, None, False)
        # from rightpanel import setListModel
        # setListModel(testObject.getData())
        return True
    else:
        return False
